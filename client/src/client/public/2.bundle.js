webpackJsonp([2],{

/***/ 771:
/*!******************************************************!*\
  !*** ./src/client/app/components/LessonPrep/List.js ***!
  \******************************************************/
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(module) {/* REACT HOT LOADER */ if (true) { (function () { var ReactHotAPI = __webpack_require__(/*! ./~/react-hot-api/modules/index.js */ 77), RootInstanceProvider = __webpack_require__(/*! ./~/react-hot-loader/RootInstanceProvider.js */ 85), ReactMount = __webpack_require__(/*! react/lib/ReactMount */ 87), React = __webpack_require__(/*! react */ 150); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } try { (function () {
	
	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(/*! react */ 150);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _reactRouter = __webpack_require__(/*! react-router */ 253);
	
	var _reactAtRest = __webpack_require__(/*! react-at-rest */ 315);
	
	var _reactBootstrap = __webpack_require__(/*! react-bootstrap */ 485);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var LessonPrepList = function (_DeliveryService) {
	    _inherits(LessonPrepList, _DeliveryService);
	
	    function LessonPrepList() {
	        _classCallCheck(this, LessonPrepList);
	
	        return _possibleConstructorReturn(this, Object.getPrototypeOf(LessonPrepList).apply(this, arguments));
	    }
	
	    _createClass(LessonPrepList, [{
	        key: 'componentWillMount',
	        value: function componentWillMount() {
	            this.store = new _reactAtRest.Store('lesson_preps');
	            this.state = {
	                count: this.store.getResource('count'),
	                perPage: 6,
	                page: this.props.params.page ? Number.parseInt(this.props.params.page) : 1
	            };
	        }
	    }, {
	        key: 'componentWillReceiveProps',
	        value: function componentWillReceiveProps(props) {
	            var pageChanged = Number.parseInt(props.params.page) !== this.state.page;
	            if (pageChanged) {
	                this.setState({ loaded: false });
	                this.stopPolling();
	                this.stopListeningToBoundResources();
	                this.boundResources = [];
	                this.bindResources(props);
	                if (this.boundResources.length) this.getResources();
	            }
	            return pageChanged;
	        }
	        // # override bindResources to load all the resources needed for this component
	
	    }, {
	        key: 'bindResources',
	        value: function bindResources(props) {
	            var page = props.params.page ? Number.parseInt(props.params.page) : 1;
	            this.setState({ page: page });
	            var query = {
	                filter: {
	                    limit: this.state.perPage,
	                    skip: this.state.perPage * (page - 1)
	                }
	            };
	            // # retrieve all the posts from the Post Store
	            this.retrieveAll(this.store, { query: query });
	        }
	    }, {
	        key: 'paginate',
	        value: function paginate(eventKey) {
	            this.props.history.push({
	                pathname: '/lessons/page/' + eventKey
	            });
	        }
	    }, {
	        key: 'render',
	        value: function render() {
	            // # show a loading message while loading data
	            if (!this.state.loaded) {
	                return _react2.default.createElement(
	                    'span',
	                    null,
	                    'Loading...'
	                );
	            }
	            var lessons = this.state.lesson_preps.map(function (prep, index) {
	                var url = _reactAtRest.Store.API_PATH_PREFIX + '/files/lesson_prep/download/default-img.png';
	                if (prep.img !== undefined) {
	                    var img = JSON.parse(prep.img);
	                    var keys = Object.keys(img);
	                    if (img.length !== 0) {
	                        url = _reactAtRest.Store.API_PATH_PREFIX + '/files/' + img[keys[0]].container + '/download/' + img[keys[0]].name;
	                    }
	                }
	                var element = [];
	                var block = _react2.default.createElement(
	                    'div',
	                    { className: 'col-md-3 agile_team_grid', key: prep.id },
	                    _react2.default.createElement(
	                        'div',
	                        { className: 'view view-sixth' },
	                        _react2.default.createElement('img', { src: url, alt: ' ', className: 'img-responsive' }),
	                        _react2.default.createElement(
	                            'div',
	                            { className: 'mask' },
	                            _react2.default.createElement(
	                                'h5',
	                                null,
	                                prep.name
	                            ),
	                            _react2.default.createElement(
	                                'p',
	                                null,
	                                prep.summary
	                            ),
	                            _react2.default.createElement(
	                                _reactRouter.Link,
	                                { to: "lessons/" + prep.id, className: 'btn btn-info' },
	                                'مشاهدة المزيد '
	                            )
	                        )
	                    ),
	                    _react2.default.createElement(
	                        'h4',
	                        null,
	                        prep.name
	                    )
	                );
	                element.push(block);
	                if (index % 4 == 3) {
	                    element.push(_react2.default.createElement('div', { className: 'clearfix' }));
	                }
	                return element;
	            });
	            var count = this.state.count._result.lesson_prep.count;
	            var totalPages = Math.floor(count / this.state.perPage);
	            return _react2.default.createElement(
	                'div',
	                { className: 'container team' },
	                lessons,
	                _react2.default.createElement(
	                    'div',
	                    { className: 'clearfix' },
	                    ' '
	                ),
	                _react2.default.createElement(_reactBootstrap.Pagination, {
	                    prev: true,
	                    next: true,
	                    first: true,
	                    last: true,
	                    ellipsis: true,
	                    boundaryLinks: true,
	                    items: totalPages,
	                    maxButtons: 5,
	                    activePage: this.state.page,
	                    onSelect: this.paginate.bind(this) })
	            );
	        }
	    }]);
	
	    return LessonPrepList;
	}(_reactAtRest.DeliveryService);
	
	exports.default = LessonPrepList;
	
	/* REACT HOT LOADER */ }).call(this); } finally { if (true) { (function () { var foundReactClasses = module.hot.data && module.hot.data.foundReactClasses || false; if (module.exports && module.makeHot) { var makeExportsHot = __webpack_require__(/*! ./~/react-hot-loader/makeExportsHot.js */ 749); if (makeExportsHot(module, __webpack_require__(/*! react */ 150))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "List.js" + ": " + err.message); } }); } } module.hot.dispose(function (data) { data.makeHot = module.makeHot; data.foundReactClasses = foundReactClasses; }); })(); } }
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(/*! ./../../../../../~/webpack/buildin/module.js */ 4)(module)))

/***/ }

});
//# sourceMappingURL=2.bundle.js.map