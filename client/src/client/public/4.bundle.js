webpackJsonp([4],{

/***/ 907:
/*!*********************************************************!*\
  !*** ./src/client/app/components/LessonPrep/Details.js ***!
  \*********************************************************/
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(module) {/* REACT HOT LOADER */ if (true) { (function () { var ReactHotAPI = __webpack_require__(/*! ./~/react-hot-api/modules/index.js */ 77), RootInstanceProvider = __webpack_require__(/*! ./~/react-hot-loader/RootInstanceProvider.js */ 85), ReactMount = __webpack_require__(/*! react/lib/ReactMount */ 87), React = __webpack_require__(/*! react */ 150); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } try { (function () {
	
	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(/*! react */ 150);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _reactAtRest = __webpack_require__(/*! react-at-rest */ 315);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var LessonPrepDetails = function (_DeliveryService) {
	    _inherits(LessonPrepDetails, _DeliveryService);
	
	    function LessonPrepDetails(props) {
	        _classCallCheck(this, LessonPrepDetails);
	
	        //  create a new Store which connected to an API at / posts
	
	        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(LessonPrepDetails).call(this, props));
	
	        _this.store = new _reactAtRest.Store('lesson_preps');
	        return _this;
	    }
	
	    _createClass(LessonPrepDetails, [{
	        key: 'bindResources',
	        value: function bindResources(props) {
	            this.retrieveResource(this.store, { id: this.props.params.id });
	        }
	    }, {
	        key: 'render',
	        value: function render() {
	            // # show a loading message while loading data
	            if (!this.state.loaded) {
	                return _react2.default.createElement(
	                    'span',
	                    null,
	                    'Loading...'
	                );
	            }
	            var lesson_prep = this.state.lesson_prep;
	
	            console.log(lesson_prep);
	            return _react2.default.createElement(
	                'div',
	                { className: 'panel panel-default', key: lesson_prep.id },
	                _react2.default.createElement(
	                    'div',
	                    { className: 'panel-heading' },
	                    _react2.default.createElement(
	                        'h3',
	                        { className: 'panel-title' },
	                        lesson_prep.name
	                    )
	                ),
	                _react2.default.createElement(
	                    'div',
	                    { className: 'panel-body' },
	                    _react2.default.createElement(
	                        'a',
	                        { className: 'btn btn-info pull-left', href: '/lessons/' + lesson_prep.id + '/edit' },
	                        'تعديل الدرس'
	                    ),
	                    lesson_prep.date
	                )
	            );
	        }
	    }]);
	
	    return LessonPrepDetails;
	}(_reactAtRest.DeliveryService);
	
	exports.default = LessonPrepDetails;
	
	/* REACT HOT LOADER */ }).call(this); } finally { if (true) { (function () { var foundReactClasses = module.hot.data && module.hot.data.foundReactClasses || false; if (module.exports && module.makeHot) { var makeExportsHot = __webpack_require__(/*! ./~/react-hot-loader/makeExportsHot.js */ 749); if (makeExportsHot(module, __webpack_require__(/*! react */ 150))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Details.js" + ": " + err.message); } }); } } module.hot.dispose(function (data) { data.makeHot = module.makeHot; data.foundReactClasses = foundReactClasses; }); })(); } }
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(/*! ./../../../../../~/webpack/buildin/module.js */ 4)(module)))

/***/ }

});
//# sourceMappingURL=4.bundle.js.map