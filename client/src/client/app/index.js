import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, browserHistory } from 'react-router';
import {Store} from 'react-at-rest';
import cookie from 'react-cookie';

import routes from './Routes';

Store.API_ENVELOPE = false;
Store.API_PATH_PREFIX = 'api';
const token = cookie.load('token', { path: '/' });
if (token !== undefined) {
    Store.SUPERAGENT_PLUGINS = [
        require('./lib/Token')(token)
    ];
}

class App extends React.Component {
    render() {
        return <Router history={browserHistory} routes={routes} />;
    }
}
ReactDOM.render(<App/>, document.getElementById('app'));
