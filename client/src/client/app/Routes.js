import Layout from './components/Layout/Index';
import LessonPrepRoute from './components/LessonPrep/Route';
import UserRoute from './components/User/Route';

const routes = [{
    path: '/',
    component: Layout,
    getIndexRoute(nextState, cb) {
        require.ensure([], function (require) {
            cb(null, { component: require('./components/Home/Index').default });
        })
    },
    childRoutes: [
        LessonPrepRoute,
        UserRoute
    ]
},
];
export default routes;