import React from 'react';
import {RestForm, Forms} from 'react-at-rest';
import {Button, Tooltip, OverlayTrigger, Panel} from 'react-bootstrap';
import FontAwesome from 'react-fontawesome';

import TextInputForm from './TextInputForm';

const AddButton = (<OverlayTrigger placement="left"
    overlay={(<Tooltip placement="top" className="in" id="tooltip-top">
        اضف درجة
    </Tooltip>) }
    >
    <Button
        bsStyle='success'
        >
        اضف  درجة
        <FontAwesome name='plus' />
    </Button>
</OverlayTrigger>);

export default class StandardForm extends RestForm {
    render() {
        return <Panel className="evalWrapper">
            <Forms.TextInput {...this.getFieldProps('name') }
                label="نص المعيار"
                />
            <Forms.Label label=" درجات المعيار"/>
            <Forms.SubFormArray
                {...this.getFieldProps('grades') }
                addResourceButton={AddButton}>
                <TextInputForm />
            </Forms.SubFormArray>
            <OverlayTrigger placement="left"
                overlay={(<Tooltip placement="top" className="in">
                    ازالة المعيار
                </Tooltip>) }
                >
                <Button
                    bsStyle='danger'
                    onClick={this.props.onRemove}>
                    ازالة  المعيار
                    <FontAwesome name='minus' />
                </Button>
            </OverlayTrigger>
        </Panel>

    }
}