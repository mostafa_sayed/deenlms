import React from 'react';
import {RestForm, Forms} from 'react-at-rest';
import {
    Button,
    Accordion,
    Panel,
    Tab,
    Grid,
    Row,
    Col,
    Tooltip,
    OverlayTrigger,
    Nav,
    NavItem
}
from 'react-bootstrap';
import {AutoAffix} from 'react-overlays';
import FontAwesome from 'react-fontawesome';

import Upload from '../../Layout/Common/form-elements/Upload';
import DateSelect from '../../Layout/Common/form-elements/DateSelect';
import TextInputForm from './SubForms/TextInputForm';
import EvaluationForm from './SubForms/EvaluationForm';

const AddButton = (<OverlayTrigger placement="left"
    overlay={(<Tooltip placement="top" className="in" id="tooltip-top">
        اضف بند
    </Tooltip>) }
    >
    <Button
        bsStyle='success'
        style={{ margin: 10 }}
        >
        <FontAwesome name='plus' />
    </Button>
</OverlayTrigger>);
export default class LessonPrepForm extends RestForm {
    render() {
      // console.log(this.props.model.field, this.state.patch.field);
        return (<form onSubmit={this.handleSubmit}>
            <Tab.Container id="left-tabs-example" defaultActiveKey="1">
                <Row className="clearfix">
                    <Col sm={2}>
                        <AutoAffix viewportOffsetTop={15}>
                            <Nav bsStyle="pills" stacked>
                                <NavItem eventKey="1">
                                    المعلومات الاساسية
                                </NavItem>
                                <NavItem eventKey="2">
                                    معرفة المفاهيم الاساسية
                                </NavItem>
                                <NavItem eventKey="3">
                                    المهمات الادائية
                                </NavItem>
                                <NavItem eventKey="4">
                                    الخبرة ومصادر التعلم
                                </NavItem>
                                <NavItem eventKey="5">
                                    التقويم
                                </NavItem>
                                <li className="active">
                                    <OverlayTrigger placement="left"
                                        overlay={(<Tooltip placement="top" className="in" id="tooltip-top">
                                            اضغظ هنا لحفظ الدرس بعد الانتهاء
                                        </Tooltip>) }
                                        >
                                        <Button type="submit" bsStyle='success'>  احفظ الدرس <FontAwesome name='floppy-o' /></Button>
                                    </OverlayTrigger>
                                </li>
                            </Nav>
                        </AutoAffix>
                    </Col>
                    <Col sm={10}>
                        <Tab.Content animation>
                            <Tab.Pane eventKey="1">
                                <Row>
                                    <Col md={6}>
                                        <Forms.TextInput {...this.getFieldProps('name') } label="اسم الدرس"/>
                                    </Col>
                                    <Col md={6}>
                                        <DateSelect {...this.getFieldProps('date') }
                                            hintText="Select Date"
                                            container="inline"
                                            autoOk={true}
                                            label="التاريخ"/>
                                    </Col>
                                </Row>
                                <Forms.TextInput {...this.getFieldProps('userid') } style={{ display: 'none' }}/>
                                <Forms.TextAreaInput {...this.getFieldProps('summary') } label="ملخص"/>
                                <Forms.SelectInput
                                    {...this.getFieldProps('grade') }
                                    label="الصف/المرحلة"
                                    options={[
                                        { id: 'kg1', name: 'روضة أولى' },
                                        { id: 'kg2', name: 'روضة ثانية' },
                                        { id: '1', name: 'الصف الأول' },
                                        { id: '2', name: 'الصف الثاني' },
                                        { id: '3', name: 'الصف الثالث' },
                                        { id: '4', name: 'الصف الرابع' },
                                        { id: '5', name: 'الصف الخامس' },
                                        { id: '6', name: 'الصف السادس' },
                                        { id: '7', name: 'الصف السابع' },
                                        { id: '8', name: 'الصف الثامن' },
                                        { id: '9', name: 'الصف التاسع' },
                                        { id: '10', name: 'الصف العاشر' },
                                        { id: '11', name: 'الصف الحادي عشر' },
                                        { id: '12', name: 'الصف الثاني عشر' }
                                    ]}/>
                                <Forms.TextInput {...this.getFieldProps('subject') } label="المادة الدراسية"/>
                                <Forms.TextInput {...this.getFieldProps('time') } label="المدة الزمنية"/>
                                <Forms.TextInput {...this.getFieldProps('topic') } label="الموضوع"/>
                                <Forms.TextAreaInput {...this.getFieldProps('notes') } label="ملاحظات"/>
                                <Upload {...this.getFieldProps('img') } container="lesson_prep" label="صورة الدرس"/>

                                 <Forms.SelectInput
                                    {...this.getFieldProps('field') }
                                    label="المجال"
                                    ref="field"
                                    options={[
                                       "الوحي الالهي",
                                       "العقيدة الاسلامية",
                                       "قيم الإسلام وآدابه",
                                       "أحكام الاسلام ومقاصده",
                                       "السيرة والشخصيات",
                                       "الهوية والقضايا المعاصرة"
                                       ]}/>
                                 <Forms.SelectInput
                                    {...this.getFieldProps('axis') }
                                    label="المحور"
                                    disabled={true}
                                    options={[
                                        { id: 'kg1', name: 'روضة أولى' },
                                        { id: 'kg2', name: 'روضة ثانية' },
                                        { id: '1', name: 'الصف الأول' },
                                        { id: '2', name: 'الصف الثاني' },
                                        { id: '3', name: 'الصف الثالث' },
                                        { id: '4', name: 'الصف الرابع' },
                                        { id: '5', name: 'الصف الخامس' },
                                        { id: '6', name: 'الصف السادس' },
                                        { id: '7', name: 'الصف السابع' },
                                        { id: '8', name: 'الصف الثامن' },
                                        { id: '9', name: 'الصف التاسع' },
                                        { id: '10', name: 'الصف العاشر' },
                                        { id: '11', name: 'الصف الحادي عشر' },
                                        { id: '12', name: 'الصف الثاني عشر' },
                                    ]}/>
                            </Tab.Pane>
                            <Tab.Pane eventKey="2">
                                <Forms.Label label="معرفة المفاهيم"/>
                                <Forms.SubFormArray
                                    {...this.getFieldProps('concepts') }
                                    wrapperClassName=""
                                    addResourceButton={AddButton}>
                                    <TextInputForm />
                                </Forms.SubFormArray>
                                <Forms.Label label="الاسئلة الاساسية"/>
                                <Forms.SubFormArray
                                    {...this.getFieldProps('questions') }
                                    wrapperClassName=""
                                    addResourceButton={AddButton}>
                                    <TextInputForm />
                                </Forms.SubFormArray>
                                <Forms.Label label="المعرفة والمهارات"/>
                                <Forms.SubFormArray
                                    {...this.getFieldProps('skills') }
                                    wrapperClassName=""
                                    addResourceButton={AddButton}>
                                    <TextInputForm />
                                </Forms.SubFormArray>
                            </Tab.Pane>
                            <Tab.Pane eventKey="3">
                                <Forms.Label label="المهمات الادائية"/>
                                <Forms.SubFormArray
                                    {...this.getFieldProps('task') }
                                    wrapperClassName="list-group"
                                    componentTagName="ul"
                                    addResourceButton={AddButton}>
                                    <TextInputForm />
                                </Forms.SubFormArray>
                                <Forms.Label label="مؤشر الاداء"/>
                                <Forms.SubFormArray
                                    {...this.getFieldProps('performanceIndicator') }
                                    wrapperClassName="list-group"
                                    componentTagName="ul"
                                    addResourceButton={AddButton}>
                                    <TextInputForm />
                                </Forms.SubFormArray>
                                <Forms.Label label="المهمات الادائية"/>
                                <Forms.SubFormArray
                                    {...this.getFieldProps('tests') }
                                    wrapperClassName="list-group"
                                    componentTagName="ul"
                                    addResourceButton={AddButton}>
                                    <TextInputForm />
                                </Forms.SubFormArray>
                            </Tab.Pane>
                            <Tab.Pane eventKey="4">
                                <Forms.Label label="تسلسل النشاطات"/>
                                <Forms.SubFormArray
                                    {...this.getFieldProps('activities') }
                                    wrapperClassName=""
                                    addResourceButton={AddButton}>
                                    <TextInputForm />
                                </Forms.SubFormArray>
                                <Forms.Label label="الفروقات الفردية"/>
                                <Forms.SubFormArray
                                    {...this.getFieldProps('individualDifferences') }
                                    wrapperClassName=""
                                    addResourceButton={AddButton}>
                                    <TextInputForm />
                                </Forms.SubFormArray>
                                <Forms.Label label="المصادر"/>
                                <Forms.SubFormArray
                                    {...this.getFieldProps('sources') }
                                    wrapperClassName=""
                                    addResourceButton={AddButton}>
                                    <TextInputForm />
                                </Forms.SubFormArray>
                            </Tab.Pane>
                            <Tab.Pane eventKey="5">
                                <Forms.Label label="التقويم"/>
                                <Forms.SubFormArray
                                    {...this.getFieldProps('evaluation') }
                                    wrapperClassName="evalsWrapper"
                                    componentTagName="div"
                                    destroyWorkaround={true}
                                    addResourceButton={(<Button
                                        bsStyle='success'
                                        style={{ margin: 10 }}
                                        >
                                        <FontAwesome name='plus' /> اضافة تقويم
                                    </Button>) }>
                                    <EvaluationForm />
                                </Forms.SubFormArray>
                            </Tab.Pane>
                        </Tab.Content>
                    </Col>
                </Row>
            </Tab.Container>
        </form>);
    }
}
