import React from 'react';
import {DeliveryService, Store} from 'react-at-rest';

export default class LessonPrepDetails extends DeliveryService {

    constructor(props) {
        super(props)
        //  create a new Store which connected to an API at / posts
        this.store = new Store('lesson_preps');
    }

    bindResources(props) {
        this.retrieveResource(this.store, { id: this.props.params.id });
    }

    render() {
        // # show a loading message while loading data 
        if (!this.state.loaded) {
            return <span>Loading...</span>
        }
        const {lesson_prep} = this.state;
        console.log(lesson_prep);
        return <div className="panel panel-default" key={lesson_prep.id}>
            <div className="panel-heading">
                <h3 className="panel-title">{lesson_prep.name}
                </h3>

            </div>
            <div className="panel-body">
                <a className="btn btn-info pull-left" href={'/lessons/' + lesson_prep.id + '/edit'} >
                    تعديل الدرس
                </a>
                {lesson_prep.date}
            </div>
        </div>
    }
}