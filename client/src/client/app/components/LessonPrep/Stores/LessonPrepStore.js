import {Store} from 'react-at-rest';

export default class LessonPrepStore extends Store {
    constructor(props) {
        super(props);
    }
    post(url, data) {
        data.evaluation = data.evaluation.filter((value) => value !== null);
        return this.ajax(url, 'POST', data);
    }
    update(url, data) {
        data.evaluation = data.evaluation.filter((value) => value !== null);
        return this.ajax(url, 'PUT', data);
    }
}