import React from 'react';
import {Link} from 'react-router';
import {DeliveryService, Store} from 'react-at-rest';
import {Pagination} from 'react-bootstrap';
import { browserHistory } from 'react-router';

export default class LessonPrepList extends DeliveryService {

    componentWillMount() {
        this.store = new Store('lesson_preps');
        this.state = {
            count: this.store.getResource('count'),
            perPage: 6,
            page: this.props.params.page ? Number.parseInt(this.props.params.page) : 1
        };
    }

    componentWillReceiveProps(props) {
        const pageChanged = (Number.parseInt(props.params.page) !== this.state.page);
        if (pageChanged) {
            this.setState({ loaded: false });
            this.stopPolling()
            this.stopListeningToBoundResources()
            this.boundResources = []
            this.bindResources(props)
            if (this.boundResources.length) this.getResources()
        }
        return pageChanged;
    }
    // # override bindResources to load all the resources needed for this component
    bindResources(props) {
        const page = props.params.page ? Number.parseInt(props.params.page) : 1
        this.setState({page});
        const query = {
            filter: {
                limit: this.state.perPage,
                skip: this.state.perPage * (page - 1)
            }
        }
        // # retrieve all the posts from the Post Store
        this.retrieveAll(this.store, { query });
    }

    paginate(eventKey) {
        this.props.history.push({
            pathname: '/lessons/page/' + eventKey,
        });
    }

    render() {
        // # show a loading message while loading data
        if (!this.state.loaded) {
            return <span>Loading...</span>
        }
        let lessons = this.state.lesson_preps.map((prep, index) => {
            let url = Store.API_PATH_PREFIX + '/files/lesson_prep/download/default-img.png';
            if (prep.img !== undefined) {
                const img = JSON.parse(prep.img);
                const keys = Object.keys(img);
                if (img.length !== 0) {
                    url = Store.API_PATH_PREFIX + '/files/' + img[keys[0]].container + '/download/' + img[keys[0]].name;
                }
            }
            const element = [];
            const block = <div className="col-md-3 agile_team_grid" key={prep.id}>
                <div className="view view-sixth">
                    <img src={url} alt=" " className="img-responsive" />
                    <div className="mask">
                        <h5>{prep.name}</h5>
                        <p>{prep.summary}</p>
                        <Link to={"lessons/" + prep.id} className="btn btn-info">مشاهدة المزيد </Link>
                    </div>
                </div>
                <h4>{prep.name}</h4>
            </div>;
            element.push(block);
            if (index % 4 == 3) {
                element.push(<div className="clearfix" />);
            }
            return element;
        })
        const count = this.state.count._result.lesson_prep.count;
        const totalPages = Math.floor(count / this.state.perPage);
        return <div className="container team">
            {lessons}
            <div className="clearfix"> </div>
            <Pagination
                prev
                next
                first
                last
                ellipsis
                boundaryLinks
                items={totalPages}
                maxButtons={5}
                activePage={this.state.page}
                onSelect={this.paginate.bind(this) } />
        </div>
    }
}
