import React from 'react';
import {RestForm, Forms, DeliveryService} from 'react-at-rest';
import cookie from 'react-cookie';
import {browserHistory} from 'react-router';
import PubSub from 'pubsub-js';

import Roles from '../../config/RoleStore';
import LessonPrepForm from './Forms/LessonPrepForm';
import LessonPrepStore from './Stores/LessonPrepStore';

export default class Form extends React.Component {
    componentDidMount() {
        Roles.matchRoles(['authorized'], '/user/login');
    }
    
    componentWillMount() {
        const {id} = this.props.params;
        this.store = new LessonPrepStore('lesson_preps');
        let userData = cookie.load('userData', { path: '/' });
        let userid;
        if (userData !== undefined) {
            userData = JSON.parse(cookie.load('userData', { path: '/' }));
            userid = userData.user.id;
        }
        this.setState({userid});
        if (id) {
            this.store.getResource(id).then((data) => {
                const model = data.lesson_prep;
                this.setState({ model, loaded: true });
            });
        } else {
            const model = { userid };
            this.setState({ model, loaded: true });
        }

    }

    handleChange(data, model) {
        // model.evaluation = model.evaluation.filter((value) => value !== null);
        // this.setState({model});
    }

    handleSuccess(data) {
        const {id} = this.props.params;
        if (data && !id) {
            const {lesson_prep} = data;
            PubSub.publish('NOTIFY', {
                autoDismiss: 3,
                dismissible: true,
                level: "success",
                message: "Lesson Created Successfuly",
                position: "br",
                title: "Lesson Created"
            });
            browserHistory.push('/lessons/' + lesson_prep.id);
        } else if (data && id) {
            const {lesson_prep} = data;
            PubSub.publish('NOTIFY', {
                autoDismiss: 3,
                dismissible: true,
                level: "success",
                message: "Lesson Updated Successfuly",
                position: "br",
                title: "Lesson Updated"
            });
            browserHistory.push('/lessons/' + lesson_prep.id);
        } else {
            PubSub.publish('NOTIFY', {
                autoDismiss: 3,
                dismissible: true,
                level: "info",
                message: "Lesson Submitted .. no changes were made",
                position: "br",
                title: "Lesson Submitted"
            });
            browserHistory.push('/lessons/' + this.state.model.id);
        }
    }

    render() {
        if (!this.state.loaded && this.props.params.id) {
            return <span>Loading...</span>
        }
        return (
            <LessonPrepForm
                onChange={this.handleChange.bind(this) }
                model={this.state.model}
                store = {this.store}
                onSuccess = {this.handleSuccess.bind(this) } />
        );
    }
}