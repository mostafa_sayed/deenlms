import React from 'react';

export default class LessonPrep extends React.Component {
    render() {
        return <div>
            {this.props.children}
        </div>;
    }
};