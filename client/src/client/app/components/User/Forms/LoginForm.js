import React from 'react';
import {RestForm, Forms} from 'react-at-rest';

export default class LoginForm extends RestForm {
    render() {
      return <form onSubmit={this.handleSubmit}>
      <Forms.TextInput {...this.getFieldProps('email')} />
      <Forms.PasswordInput {...this.getFieldProps('password') } />
      <button type='submit'>Save</button>
    </form>        
    }
}