import React from 'react';
import cookie from 'react-cookie';
import { browserHistory } from 'react-router';
import PubSub from 'pubsub-js';

import LogoutStore from './Stores/LogoutStore';

export default class Logout extends React.Component {
    constructor(props) {
        super(props);
        this.logoutStore = new LogoutStore('users');
    }
    handleLogout() {
        cookie.remove('token', { path: '/' });
        cookie.remove('userData', { path: '/' });
        this.logoutStore.createResource();
        PubSub.publish( 'NOTIFY', {
            autoDismiss: 3,
            dismissible: true,
            level: "info",
            message: "You've been successfully Logged out",
            position: "br",
            title: "Logged out"
        });
        PubSub.publish( 'LOGGED_OUT', '');
        if (this.props.redirect) {
            browserHistory.push(this.props.redirect);
        } else {
            browserHistory.push('/');
        }
    }

    render() {
        return <div>
            <button className="btn btn danger"
                onClick={this.handleLogout.bind(this) }
                >
                Logout
            </button>
        </div>;
    }
}