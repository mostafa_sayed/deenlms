import React from 'react';

export default class Contact extends React.Component {
    render() {
        return <div className="contact" id="contact">
        {/* //contact */}
        <div className="col-md-6 w3agile_contact_left">
          <h3>Contact Us</h3>
          <p>Excepteur sint occaecat cupidatat non proident, sunt 
            in culpa qui officia deserunt mollit anim id est laborum reprehenderit in voluptate velit esse 
            cillum dolore eu fugiat nulla pariatur.</p>
          <form action="#" method="post">
            <input type="text" name="Name" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" required />
            <input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required />
            <textarea name="Message" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message...';}" required defaultValue={"Message..."} />
            <input type="submit" value="Send" />
          </form>
        </div>
        <div className="col-md-6 w3agile_contact_right">
          <h3><a href="index.html">Schooling</a></h3>
          <div className="col-xs-6 w3agile_contact_right_agileinfo">
            <h4>Address</h4>
            <p><span className="glyphicon glyphicon-map-marker" aria-hidden="true" />8921 California Long Beach <i>PO Box 8921 202 East Ocean.</i></p>
            <p><span className="glyphicon glyphicon-earphone" aria-hidden="true" />(+) 0983 010 823</p>
            <p><span className="glyphicon glyphicon-envelope" aria-hidden="true" /><a href="mailto:info@example.com">info@example.com</a></p>
          </div>
          <div className="col-xs-6 w3agile_contact_right_agileinfo">
            <h4>Follow Us</h4>
            <div className="agileits_social_icons">
              <a href="#" className="icon-button twitter"><i className="icon-twitter" /><span /></a>
              <a href="#" className="icon-button facebook"><i className="icon-facebook" /><span /></a>
              <a href="#" className="icon-button google-plus"><i className="icon-google-plus" /><span /></a>
            </div>
          </div>
          <div className="clearfix"> </div>
          <div className="w3_copy_right">
            <p>© 2016 Schooling. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
          </div>
        </div>
        <div className="clearfix"> </div>
      </div>
      {/* //contact */}
    }
}