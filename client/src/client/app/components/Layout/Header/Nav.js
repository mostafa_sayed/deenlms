import React from 'react';
import {Link, withRouter} from 'react-router';
import routes from '../../../Routes.js';
import ACL from '../Common/ACL';

class Nav extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return <div className="navigation">
            <div className="container">
                <div className="logo">
                    <h1><a href="/">Schooling</a></h1>
                </div>
                <div className="navigation-right">
                    <span className="menu"><img src="/public/images/menu.png" alt=" " /></span>
                    <nav className="link-effect-3" id="link-effect-3">
                        <ul className="nav1 nav nav-wil">
                            <li className={this.props.router.isActive('/', true) ? 'active' : ''} >
                                <Link to="/" data-hover="الرئيسية" className="scroll">الرئيسية</Link>
                            </li>
                            <li className={this.props.router.isActive('/lessons') ? 'active' : ''}>
                                <Link to="lessons" data-hover="تحضيرات الدروس" className="scroll">تحضيرات الدروس</Link>
                            </li>
                            <ACL roles={['guest']} WrapperTag='li' WrapperProps={{ className: this.props.router.isActive('/user') ? 'active' : '' }} >
                                <Link to="user/login" data-hover="التسجيل / تسجيل الدخول" className="scroll">التسجيل / تسجيل الدخول</Link>
                            </ACL>
                        </ul>
                    </nav>
                </div>
                <div className="clearfix" />
            </div>
        </div >
    }
}
export default withRouter(Nav);