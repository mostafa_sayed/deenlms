import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import {Grid, Row, Col} from 'react-bootstrap';

import Contact from './Contact';
import Nav from './Header/Nav';
import Login from '../User/Login';
// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();
import Notify from './Common/Notify';

export default class Layout extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return <MuiThemeProvider>
            <div>
                <Notify />
                <Grid fluid>
                    <Row className="show-grid">
                        <Col xs={12} md={12}>
                            <Nav />
                        </Col>
                    </Row>
                    <Row className="show-grid" style={{marginTop: 15}}>
                        <Col xs={12} md={12}>
                            {this.props.children}
                        </Col>
                    </Row>
                </Grid>
            </div>
        </MuiThemeProvider>
    }
}
