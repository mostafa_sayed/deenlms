import React from 'react';
import NotificationSystem from 'react-notification-system';
import PubSub from 'pubsub-js';

export default class Notify extends React.Component {
    _addNotification(msg, data) {
        this.refs.notificationSystem.addNotification(data);
    }
    componentDidMount() {
        PubSub.subscribe("NOTIFY", this._addNotification.bind(this));
    }
    render() {
        return (
            <div>
                <NotificationSystem ref="notificationSystem" />
            </div>
        );
    }
}