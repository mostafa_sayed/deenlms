import React from 'react';
import {Store} from 'react-at-rest';
import PubSub from 'pubsub-js';

import FileStore from './FileStore';
const styles = {
    wrapper: {
        width: 150,
        height: 150
    },
    img: {
        width: '100%',
        height: '100%'
    }
}
module.exports = class FileTpl extends React.Component {
    constructor(props) {
        super(props);
        this.store = new FileStore('files');
        this.state = { deleted: false };
    }
    handleDelete(e) {
        e.preventDefault();
        const {img} = this.props;
        this.store.destroyResource(img.container + '/files/' + img.name)
            .then((data) => {
                this.setState({ deleted: true });
                PubSub.publish('FILE_DELETED', img);
            });
    }

    render() {
        if (this.state.deleted) {
            return null
        }
        return <div style={styles.wrapper}>
            <button onClick={this.handleDelete.bind(this) } > Delete Image </button>
            <img style={styles.img} src={Store.API_PATH_PREFIX + '/files/' + this.props.img.container + '/download/' + this.props.img.name} />
        </div>
    }
}