import {Store} from 'react-at-rest';
import cookie from 'react-cookie';
import Request from 'superagent';
import PubSub from 'pubsub-js';

export default class FileStore extends Store {
    constructor(props) {
        super(props);
    }

    post(url, options) {
        const token = cookie.load('token', { path: '/' });
        return new Promise((resolve, reject) => {
            const req = Request.post(url + '/' + options.container + '/upload');
            if (token !== undefined) {
                req.use(require('../../../../../lib/Token')(token.id));
            }
            req.field('file', options.file)
                .on('progress', function (e) {
                    PubSub.publish('FILE_UPLOADING', { percentage: e.percent, index: options.index });
                })
                .end(function (err, res) {
                    PubSub.publish('FILE_UPLOADED', { result: res, index: options.index });
                });
        });
    }
}