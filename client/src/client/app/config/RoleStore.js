import React from 'react';
import {browserHistory} from 'react-router';
import cookie from 'react-cookie';
import PubSub from 'pubsub-js';

class RoleStore extends React.Component {
    getCookies() {
        let data = [];
        const token = cookie.load('token', { path: '/' });
        data['token'] = token === undefined ? false : token;
        if (data['token']) {
            data['userData'] = JSON.parse(cookie.load('userData', { path: '/' }));
        }
        return data;
    }

    guest() {
        const cookies = this.getCookies();
        return cookies['token'] ? false : true;
    }

    authorized() {
        const cookies = this.getCookies();
        return cookies['token'] ? true : false;
    }

    matchRoles(roles, redirect = null) {
        let auth = false;
        for (var value of roles) {
            if (this[value]() === true) {
                auth = true;
            }
        }
        if (auth === false && redirect) {
            PubSub.publish( 'NOTIFY', {
            autoDismiss: 3,
            dismissible: true,
            level: "info",
            message: "ليس لديك التصريحات اللازمة لرؤية هذه الصفحة",
            position: "br",
            title: "غير مسموح"
        });
            browserHistory.push(redirect);
        }
        return auth;
    }
}

export default new RoleStore();